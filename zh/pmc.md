## OpenHarmony项目管理委员会（PMC）


   OpenHarmony项目管理委员会（PMC: Project Management Committee）负责OpenHarmony社区管理，职责如下：
1. 负责社区管理工作，包括开源社区版本规划、架构看护、特性代码开发维护、版本及补丁规划等；
2. 发布和处理社区需求，为开源社区提供技术架构指导和技术决策；
3. 组织社区安全工作，及时进行安全漏洞扫描、响应、处理等工作；
4. 处理社区Bug、issue、邮件列表，闭环周期满足开源社区的SLA要求；
5. 负责PMC、Committer成员的选举和退出；制定PMC、Committer协作机制；


## OpenHarmony PMC成员列表
| 姓名 | 账号   | 角色 | 领域 |
| :----: | :----: | :----: | :----: |
| 李毅 | nicholas-li | PMC主任 | 总架构 |
| 董金光 | dongjinguang | PMC成员 | 系统架构 |
| 付天福 | futianfu | PMC成员 |	安全架构 |
| 万承臻 | wanchengzhen | PMC成员 | 系统架构 |
| 马耀辉 | stesen | PMC成员 | DFX设计子系统 |
| 陈  风 | chenfeng469 | PMC成员 | HDF统一驱动框架 |
| 鲁  波 | borne | PMC成员 |	轻量级ACE |
| 王  振 | wangzhen | PMC成员 | 用户程序框架/元能力 |
| 尹友展 | yinyouzhan | PMC成员 | 公共通信 |
| 李加润 | lijiarun | PMC成员 | 分布式任务调度 |
| 谭利文 | tanliwen1 | PMC成员 |	多媒体 |
| 王  兴 | wangxing-hw | PMC成员 | 编译构建 |
| 肖  峰 | blue.xiaofeng | PMC成员 |	公共基础库 |
| 张明修 | zmx1104 | PMC成员 | 社区版本发布 |
| 袁文鸿 | yuan.w.hong | PMC成员 | 芯片组件 |


## PMC会议链接
- 会议时间: 每双周周一 16:30-17:30
- 会议主题: 通过邮件通知
- 请[订阅](https://lists.openatom.io/postorius/lists/cicd.openharmony.io)邮件列表 dev@openharmony.io 获取会议链接

## 联系方式

| 地址                                 | 简介        | 用途说明                                                         |
| ---------------------------------------|---------- | ------------------------------------------------------------ |
| dev@openharmony.io  <img width=120/>| 开发邮件列表 <img width=100/> | OpenHarmony社区开发讨论邮件列表，任何社区开发相关话题都可以在邮件列表讨论。任何开发者可[订阅](https://lists.openatom.io/postorius/lists/dev.openharmony.io)。<img width=200/>|
| cicd@openharmony.io <img width=120/> | CI邮件列表  <img width=100/>| OpenHarmomny CICD构建邮件列表，任何开发者可[订阅](https://lists.openatom.io/postorius/lists/cicd.openharmony.io)。<img width=200/>|
| pmc@openharmony.io  <img width=120/>| PMC邮件列表  <img width=100/>| PMC讨论邮件列表，PMC成员可[订阅](https://lists.openatom.io/postorius/lists/pmc.openharmony.io/)。<img width=200/>|

