## SIG组工作目标和范围

### 工作目标
维护三方引入仓littlefs，支撑M核文件系统商用
### 工作范围
鸿蒙在轻设备M核上的文件系统的应用
## 代码仓
- 代码仓地址：
  - repository1名称：https://gitee.com/openharmony/third_party_littlefs

## SIG组成员

### Leader
- [@wangmihu2008](https://gitee.com/wangmihu2008)

### Committers列表
- [@li_zan](https://gitee.com/li_zan)
